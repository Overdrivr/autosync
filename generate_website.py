from jinja2 import Template
import shutil

exec(open("./autosync/config.py").read())

with open('website/index.html') as f:
    template = Template(f.read())

urls = {
    'win10': 'https://storage.googleapis.com/captionpal-releases/{}/captionpal-Windows-10-10.0.14393-SP010.exe'.format(__version__),
    'ubuntu18': 'https://storage.googleapis.com/captionpal-releases/{0}/captionpal-Linux-4.15.0-1025-gcp-x86_64-with-Ubuntu-18.04-bionic4.15.0-1025-gcp-{0}.tar.gz'.format(__version__)
}

print(urls)

contents = template.render(
    download_url_win10=urls['win10'],
    download_url_ubuntu18=urls['ubuntu18'],
    captionpal_version=__version__
)

with open('generated/index.html', 'w') as f:
    f.write(contents)

shutil.copy('logo.png', 'generated/logo.png')
shutil.copy('website/caption_pal_demo.mp4', 'generated/caption_pal_demo.mp4')
shutil.copy('website/dashboard.txt', 'generated/dashboard.txt')
