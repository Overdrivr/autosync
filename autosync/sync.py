import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt
from scipy.signal import medfilt
from scipy.ndimage.interpolation import shift
from scipy.ndimage import zoom
from sklearn.metrics import log_loss
import scipy
import warnings

from autosync.dataset import *
from autosync.utils import *

def sync(movie, sub, save_to=None):
    '''
    Synchronizes subtitle with movie's audio

    Attributes:
        movie       Filename of the media to extract audio from
        sub         Filename of the subtitle to synchronize
        save_to     Export filename for the synced subtitle.
    '''
    yield 'Extracting audio...'
    test_audio = fetch_dataset_slices(movie, test_percent=0, store_wav_file=False, globbing=False)

    yield 'Analyzing audio...'
    folder = os.path.dirname(os.path.dirname(__file__))
    graph = tf.Graph()
    with graph.as_default():
        with tf.Session() as sess:
            meta_graph_def = tf.saved_model.loader.load(
                sess,
                [tf.saved_model.tag_constants.SERVING],
                os.path.join(folder, 'saved_models', 'm17_dataclean_300k_balanced'),
            )
            signature = meta_graph_def.signature_def
            signature_key = tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY
            print('Model signature:', signature)
            x_tensor_name = signature[signature_key].inputs['audio_mfcc'].name
            y_tensor_name = signature[signature_key].outputs['has_speech'].name

            x = sess.graph.get_tensor_by_name(x_tensor_name)
            y = sess.graph.get_tensor_by_name(y_tensor_name)

            result = sess.run(y, feed_dict={
                x: test_audio,
            })


    block = 0.032
    block_ms = block * 1000
    framerate = 16000
    block_len = int(block_ms * framerate / 1000)

    xaxis = np.arange(0, result.shape[0], 1)
    xaxis = xaxis * block

    yield 'Parsing subtitle...'
    speech_prob = result[:, 0]
    sub_prob = sub_to_sequence(sub, fps=16000, total_length=speech_prob.shape[0] * block_len)
    sub_prob = sub_prob.reshape((sub_prob.shape[0] // block_len, block_len))
    sub_prob = contains_speech(sub_prob, block_ms)
    sub_prob = sub_prob[:, 0]

    def loss_fn_factory(speech, sub):
        def loss(params):
            #import pdb; pdb.set_trace()
            delay, fps = params
            delay = int(delay)

            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                # Hypothesis is 1st order performs better than default 3rd order
                sub_delayed = zoom(sub, fps, order=1)
                # Rounding avoid truncation when performing type conversion to uint8 below
                # And results in a more accurate result
                sub_delayed = np.round(sub_delayed)

            # Zoom has reduced array size. Pad with 0s at the end
            if sub_delayed.shape[0] < sub.shape[0]:
                diff = sub.shape[0] - sub_delayed.shape[0]
                sub_delayed = np.pad(sub_delayed, (0, diff), 'constant')
            # Zoom has extended array size. Crop the end
            elif sub_delayed.shape[0] > sub.shape[0]:
                sub_delayed = sub_delayed[:sub.shape[0]]

            #sub_delayed = np.roll(sub, delay, axis=0)
            sub_delayed = shift(sub_delayed, delay)
            sub_delayed = sub_delayed.astype(np.uint8)
            #if delay > 0:
            #    sub_delayed[:delay] = 0
            #elif delay < 0:
            #    sub_delayed[delay:] = 0
            l = log_loss(sub_delayed, speech)
            print('Loss: {:.2f} | Delay: {} | FPS ratio: {:.2f}'.format(l, delay, fps))
            return l
        return loss

    # Discretize sub
    sub_prob[sub_prob > 0] = 1
    sub_prob[sub_prob < 1] = 0
    sub_prob = sub_prob.astype(np.float64)
    speech_prob = speech_prob.astype(np.float64)
    loss_fn = loss_fn_factory(speech_prob, sub_prob)

    # fmin_l_bfgs_b can be stuck in local minima
    # Which makes the quality of the result highly dependent on the initial conditions
    #result = scipy.optimize.fmin_l_bfgs_b(loss_fn, 1000, approx_grad=True, epsilon=1)
    #delay, final_loss, info = result

    # basinhopping gives random, sometimes inacurate results
    #result = scipy.optimize.basinhopping(loss_fn, 0, T=5000, stepsize=1000)
    #delay = result['x']

    yield 'Synchronizing...'

    # Brute force match to search for a global minimum
    # We use a time span of 3 minutes with a 1s step
    # (3 minutes in case there is a TV show summary that is not in the subs)
    span_s = 3 * 60
    span = span_s / block
    res = 3.0 / block
    rrange = (slice(-span , span, res), slice(0.9, 1.1, 0.05))
    print('Range: {}'.format(rrange))
    result = scipy.optimize.brute(loss_fn, rrange)
    delay, ratio = result

    print('Optimization result (blocks | ratio): {}'.format(result))

    delay *= block

    print('Sub is off by {}s'.format(delay))
    subs = resync_sub(sub, offset=int(delay), ratio=ratio)

    if save_to is None:
        filename, ext = os.path.splitext(movie)
        save_to = filename + '_synced.srt'

    subs.save(save_to, encoding='utf-8')

    msg = (
        "Subtitle synced (delay: {:.2f}s | fps: {:.2f}) and saved.\n"\
        "Just open VLC, select it in the sub list and you're all set.\n"
        "File: {}"
    ).format(delay, ratio, save_to)

    yield msg


def main():
    movie = "C:/Users/remib/Downloads/Game.of.Thrones.Season.6.720p.BluRay.x264.ShAaNiG/Game.of.Thrones.S06E04.720p.BluRay.x264.ShAaNiG.mkv"
    sub = "C:/Users/remib/Downloads/Game.of.Thrones.Season.6.720p.BluRay.x264.ShAaNiG/Game of Thrones - 06x04 - Book of the Stranger.AVS.TRANS.French.HI.C.updated.Addic7ed.com.srt"
    for step in sync(movie, sub):
        print('Step: ', step)

if __name__ == '__main__':
    main()
