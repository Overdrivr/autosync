import argparse

def parser():
    p = argparse.ArgumentParser(
        description='CaptionPal - Fetch and Synchronize subtitles automatically using ML.',
        )

    p.add_argument(
        'video',
        type=str,
        help='Path to video file',
    )

    p.add_argument(
        '--sub',
        type=str,
        help='Subtitle to synchronize with video file',
        default=None,
        dest='sub',
    )

    p.add_argument(
        '--lang',
        type=str,
        help='Language of the subtitle to retrieve',
        default='english',
        dest='lang',
    )

    return p

def parse_args(argv=None):
    p = parser()
    return vars(p.parse_args(args=argv))
