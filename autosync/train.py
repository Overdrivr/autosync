import tensorflow as tf
from tensorflow import keras
import numpy as np
from autosync.dataset import *
from tensorflow.keras.utils import plot_model
from tqdm import tqdm

def train(dir='.'):
    (train_audio, train_subs), (test_audio, test_subs) = fetch_dataset_slices(dir)
    np.save('cache/train_audio.npy', train_audio)
    np.save('cache/train_subs.npy', train_subs)
    np.save('cache/test_audio.npy', test_audio)
    np.save('cache/test_subs.npy', test_subs)
    '''
    train_audio = np.load('cache/train_audio.npy')
    train_subs = np.load('cache/train_subs.npy')
    test_audio = np.load('cache/test_audio.npy')
    test_subs = np.load('cache/test_subs.npy')
    '''
    print('{} items in training, {} items in test'.format(
        len(train_audio),
        len(test_audio),
        )
    )

    dataset_balance = np.count_nonzero(train_subs) / train_subs.shape[0] * 100
    print('Training dataset balance: {}%'.format(dataset_balance))

    dataset_balance = np.count_nonzero(test_subs) / test_subs.shape[0] * 100
    print('Test dataset balance: {}%'.format(dataset_balance))

    epochs = 200
    batch_size = 32
    # m6 is just feat normalized
    model_id = 'm17_dataclean_300k_balanced'
    logsess = 'logs/' + model_id

    training_set = create_dataset(train_audio, train_subs)
    training_set = training_set.repeat(epochs).batch(batch_size)


    test_set = create_dataset(test_audio, test_subs)
    test_set = test_set.repeat(epochs).batch(len(test_subs))

    # Create a training set for evaluation purposes only of the same length than test dataset
    fit_set = create_dataset(train_audio[:len(test_subs)], train_subs[:len(test_subs)])
    fit_set = fit_set.repeat(epochs).batch(len(test_subs))

    input_shape = (train_audio.shape[1], 1)
    print('Building model... {}'.format(input_shape))

    '''
    model = keras.Sequential([
        keras.layers.Conv1D(filters=12, kernel_size=(3), activation='relu', input_shape=input_shape),
        keras.layers.Conv1D(filters=12, kernel_size=(3), activation='relu'),
        keras.layers.Conv1D(filters=12, kernel_size=(3), activation='relu'),
        keras.layers.Flatten(),
        keras.layers.Dense(3, activation="relu"),
        keras.layers.Dense(1, activation="tanh"),
        keras.layers.Lambda(lambda x: (x + 1.0) / 2.0),
    ])
    '''

    model = keras.Sequential([
        #keras.layers.Flatten(input_shape=input_shape),
        #keras.layers.Dense(28, activation="elu"),

        keras.layers.Conv1D(filters=14, kernel_size=(3), activation='relu', input_shape=input_shape),
        keras.layers.Conv1D(filters=14, kernel_size=(3), activation='relu', input_shape=input_shape),
        keras.layers.BatchNormalization(),
        keras.layers.Flatten(),

        keras.layers.Dense(56, activation="relu"),
        keras.layers.BatchNormalization(),

        keras.layers.Dense(6, activation="relu"),
        #keras.layers.BatchNormalization(),

        keras.layers.Dense(1, activation="tanh"),
        keras.layers.Lambda(lambda x: (x + 1.0) / 2.0),
        #keras.layers.Conv1D(filters=12, kernel_size=(3), activation='relu'),
        #keras.layers.Dense(28, activation="relu"),

        #keras.layers.Dense(56, activation="relu"),
        #keras.layers.Dropout(0.2),

        #keras.layers.BatchNormalization(),
    ])

    #plot_model(model, to_file='model.png', show_shapes=True)

    # Optimizer
    optimizer = tf.train.AdamOptimizer(
        learning_rate=0.001,
    )

    # Training loss
    iterator = training_set.make_one_shot_iterator()
    features, labels = iterator.get_next()
    predicted_labels = model(features)
    loss = tf.keras.losses.binary_crossentropy(labels, predicted_labels)
    loss = tf.reduce_mean(loss)
    tf.summary.scalar('loss', loss)
    merged = tf.summary.merge_all()

    # Train accuracy
    iterator = fit_set.make_one_shot_iterator()
    features, labels = iterator.get_next()
    predicted_labels = model(features)
    _pl = tf.cast(tf.round(predicted_labels), tf.int32)
    _l = tf.cast(labels, tf.int32)
    acc_train = tf.contrib.metrics.accuracy(
        _pl,
        _l
    )
    accuracy_value_train = tf.placeholder(tf.float32, shape=())
    accuracy_summary_train = tf.summary.scalar('accuracy:train', accuracy_value_train)

    # Test accuracy
    iterator = test_set.make_one_shot_iterator()
    features, labels = iterator.get_next()
    predicted_labels = model(features)
    _pl = tf.cast(tf.round(predicted_labels), tf.int32)
    _l = tf.cast(labels, tf.int32)
    acc = tf.contrib.metrics.accuracy(
        _pl,
        _l
    )
    accuracy_value_ = tf.placeholder(tf.float32, shape=())
    accuracy_summary = tf.summary.scalar('accuracy:test', accuracy_value_)

    # Trainable variables
    train_vars = model.trainable_variables
    print('Found {} vars to train'.format(len(train_vars)))

    # Train op
    train_op = optimizer.minimize(loss, var_list=train_vars)

    i = 0
    ei = 0
    epoch_count = 0
    with tf.Session() as sess, tqdm(total = epochs*len(train_audio)) as pbar:
        # Summaries
        writer = tf.summary.FileWriter(logsess, sess.graph)

        # Train
        sess.run(tf.global_variables_initializer())

        try:
            while True:
                i += batch_size
                ei += batch_size
                # Train & evaluate
                _, loss_value, summary = sess.run([train_op, loss, merged])

                writer.add_summary(summary, i)

                # Update progress bar
                pbar.set_postfix_str("loss={:.3f}".format(loss_value))
                pbar.refresh()
                pbar.update(batch_size)

                if ei < len(train_audio):
                    continue

                ei -= len(train_audio)
                epoch_count += 1

                # Test train accuracy after each epoch
                acc_value, = sess.run([acc_train])
                print('Train acc (epoch: {}): {}'.format(epoch_count, acc_value))
                summary = sess.run(accuracy_summary_train, feed_dict={accuracy_value_train: acc_value})
                writer.add_summary(summary, i)

                # Test accuracy after each epoch
                acc_value, label_value, predicted_value, = sess.run([acc, labels, predicted_labels])
                print('Test acc (epoch: {}): {}'.format(epoch_count, acc_value))
                summary = sess.run(accuracy_summary, feed_dict={accuracy_value_: acc_value})
                writer.add_summary(summary, i)
                '''
                pred_bin = np.zeros(predicted_value.shape) + 0.1
                pred_bin[predicted_value > 0.5] = 0.9
                plt.plot(label_value)
                plt.plot(predicted_value)
                plt.plot(pred_bin)
                plt.show()
                '''
        except tf.errors.OutOfRangeError:
            inputs = {
                "audio_mfcc": features
            }
            outputs = {
                "has_speech": predicted_labels
            }
            tf.saved_model.simple_save(
                sess, 'saved_models/' + model_id, inputs, outputs
            )

        # TODO: test accuracy
        # TODO: Check acc function is correct
        # TODO:
    '''
    test_acc = model.evaluate(
        test_set,
        steps=len(test_audio) // batch_size
    )

    print('Acc:', test_acc)
    '''
    #tf.keras.models.save_model(model, 'saved_models/v6')
    #saved_model_path = tf.contrib.saved_model.save_keras_model(model, "./saved_models")

    '''
    print('Fitting model')
    for i in range(1):
        history = model.fit(
            training_set.make_one_shot_iterator(),
            steps_per_epoch=len(train_audio) // batch_size,
            epochs=2,
            verbose=1)

        test_acc = model.evaluate(
            test_set,
            steps=len(test_audio) // batch_size
        )

        print('Acc:', test_acc)
        saved_model_path = tf.contrib.saved_model.save_keras_model(model, "./saved_models")
    '''

def main():
    dir = ["datasets/10*.avi", "datasets/Game*.mkv"]
    print('Beginning training, data dir: {}'.format(dir))
    train(dir=dir)

if __name__ == '__main__':
    main()
