#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QPushButton, QLabel, QHBoxLayout, QVBoxLayout, QFileDialog, QComboBox, QTabWidget, QSpacerItem, QSizePolicy, QProgressBar
from PyQt5.QtGui import QIcon, QPixmap, QPainter, QImage, QFont, QPalette, QColor, QDesktopServices
from PyQt5.QtCore import QSize, Qt, QRectF, QPoint, QUrl
import sys, random
from os import path
import os
from threading import Thread
from guessit import guessit
from addic7ed_cli.episode import search
from addic7ed_cli.request import session
import tempfile
import uuid
import logging
from multiprocessing import freeze_support
import chardet

from autosync.argparser import parse_args
from autosync.config import __version__, __subslangs__

def sync_in_thread(movie, sub, cb, save_to='sub.srt'):
    # Lazy import. Because importing TF is no joke, takes several seconds
    from autosync.sync import sync
    try:
        for step in sync(movie, sub, save_to=save_to):
            cb(step)
    except Exception as e:
        msg = 'Error: {}'.format(e)
        print(e)
        cb(msg)

def fetch_sub_if_found(info, lang, save_to='subtitle.srt'):
    if info['type'] == 'episode':
        canonical = '{} {}x{}'.format(
            info['title'],
            info['season'],
            info['episode']
        )
        result = search(canonical)

        if result is None or len(result) == 0:
            return None

        # What to do if more than 1 result is available ?
        result = result[0]

        result.fetch_versions()
        # TODO: Support hearing impaired
        versions = result.filter_versions([lang], completed=True)

        # Use first version, we're gonna sync it anyway
        version = versions[0]

        res = session.get(version.url)
        content = res.content
        # Fetch contents encoding from response metadata
        encoding = res._response.apparent_encoding

        if content[:9] == '<!DOCTYPE':
            raise FatalError('Daily Download count exceeded.')

        if isinstance(content, bytes):
            try:
                # Decode with response's encoding
                sub = content.decode(encoding)
            except (UnicodeDecodeError, LookupError) as e:
                # If that fails, try to autodetect and try decode with that
                encoding = chardet.detect(content)
                print('Encoding detection: {}'.format(encoding))
                sub = content.decode(encoding['encoding'])

        sub = sub.encode('utf-8')

        with open(save_to, 'wb') as fp:
            fp.write(sub)

        return True

class MainWindow(QWidget):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.mainAreaWidget = QWidget(self)

        # Data
        self.movie = None
        self.sub = None
        self.active_path = '/home'
        self.media_guessed_info = None
        self.temp_storage = tempfile.TemporaryDirectory()
        langs = __subslangs__

        # Widgets
        large_font = QFont("Roboto", 24, QFont.Light)
        mid_font = QFont("Roboto", 14, QFont.Light)
        small_font = QFont("Roboto", 10, QFont.Light)

        self.main_label = QLabel("CaptionPal", parent=self)
        self.main_label.setFont(large_font)
        self.version_label = QLabel(__version__, parent=self)
        self.version_label.setFont(small_font)
        self.sub_label = QLabel("Automatic subtitle download and synchronization", parent=self)
        self.sub_label.setFont(mid_font)

        self.tabs = QTabWidget(parent=self)
        self.tab_auto = QWidget(parent=self.tabs)
        self.tab_manual = QWidget(parent=self.tabs)

        self.movie_button = QPushButton("Pick video file", parent=self)
        self.movie_button.setStyleSheet("padding: 20px;")
        self.movie_button.setFont(mid_font)
        #self.selected_movie_label = QLabel("No file selected", parent=self)

        self.fetch_subtitle_button = QPushButton("Download and Sync.", parent=self.tab_auto)
        self.lang_dropdown = QComboBox(parent=self.tab_auto)
        [self.lang_dropdown.addItem(l) for l in langs]
        #self.fetch_sub_status_label = QLabel("No subtitle yet.", parent=self.tab_auto)

        self.sub_button = QPushButton("Pick subs", parent=self.tab_manual)
        self.selected_sub_label = QLabel("No subtitle file selected", parent=self.tab_manual)
        self.start_button = QPushButton("Sync", parent=self.tab_manual)

        self.status_label = QLabel("Select a video file to get started.", parent=self)
        self.progress_bar = QProgressBar(self)

        # Footer
        self.website_label = QLabel('<a href="https://captionpal.org">website</a>')
        self.gitlab_label = QLabel('<a href="https://gitlab.com/Overdrivr/autosync">source code</a>')
        self.bug_report_label = QLabel(
            '<a href="mailto:incoming+overdrivr-autosync-11281288-issue-@incoming.gitlab.com">report a bug</a>'
        )
        self.subbers_label = QLabel('<a href="https://www.patreon.com/addic7ed">addic7ed (tv-subs)</a>')

        # Actions
        self.movie_button.clicked.connect(self.load_movie)
        self.sub_button.clicked.connect(self.load_sub)
        self.start_button.clicked.connect(self.sync)
        self.fetch_subtitle_button.clicked.connect(self.find_and_sync)
        self.gitlab_label.linkActivated.connect(self.link)
        self.bug_report_label.linkActivated.connect(self.link)
        self.website_label.linkActivated.connect(self.link)
        self.subbers_label.linkActivated.connect(self.link)

        # Layout
        self.main_layout = QVBoxLayout()
        self.main_layout.addWidget(self.main_label)
        self.main_layout.addWidget(self.sub_label)
        self.main_layout.addWidget(self.version_label)

        ## row - movie file + movie name
        self.movie_row = QVBoxLayout()
        self.movie_row.setAlignment(Qt.AlignCenter)
        self.movie_row.addWidget(self.movie_button)
        #self.movie_row.addWidget(self.selected_movie_label)
        self.main_layout.addSpacerItem(QSpacerItem(0, 20))
        self.main_layout.addLayout(self.movie_row)
        self.main_layout.addSpacerItem(QSpacerItem(0, 20))

        ## Tabs
        self.tab_auto_layout = QVBoxLayout()
        self.tab_manual_layout = QVBoxLayout()

        ## row - fetch subtitle
        self.autofetch_row = QHBoxLayout()
        self.autofetch_row.addWidget(self.lang_dropdown)
        self.autofetch_row.addWidget(self.fetch_subtitle_button)
        #self.autofetch_row.addWidget(self.fetch_sub_status_label)
        self.tab_auto_layout.addLayout(self.autofetch_row)

        ## row - subtitle file + subtitle name
        self.sub_row = QHBoxLayout()
        self.sub_row.addWidget(self.sub_button)
        self.sub_row.addWidget(self.selected_sub_label)
        self.tab_manual_layout.addLayout(self.sub_row)
        self.tab_manual_layout.addWidget(self.start_button)

        ## Construct tabs
        self.tab_auto.setLayout(self.tab_auto_layout)
        self.tab_manual.setLayout(self.tab_manual_layout)
        self.tabs.addTab(self.tab_auto, "Full-auto")
        self.tabs.addTab(self.tab_manual, "Already have a sub ?")

        self.main_layout.addWidget(self.tabs)
        self.main_layout.addWidget(self.status_label)
        self.main_layout.addWidget(self.progress_bar)

        ## Footer
        footer_row = QHBoxLayout()
        footer_row.addStretch()
        footer_row.addWidget(self.website_label)
        footer_row.addWidget(self.bug_report_label)
        footer_row.addWidget(self.gitlab_label)
        footer_row.addWidget(self.subbers_label)

        self.main_layout.addLayout(footer_row)

        self.setLayout(self.main_layout)

    def load_movie(self):
        text, ok = QFileDialog.getOpenFileName(self, 'Open video file', self.active_path)
        if text is None or text is "":
            return
        # Retain file dialog location for next time
        self.active_path = os.path.dirname(text)

        print('Movie file: {}'.format(text))
        self.movie = text
        filename = os.path.basename(self.movie)
        #self.selected_movie_label.setText(filename)

        self.media_guessed_info = guessit(self.movie)
        print('Guessed information: {}'.format(self.media_guessed_info))

    def init_progress(self, nsteps=7):
        '''Resets progress bar'''
        self.total_steps = nsteps
        self.progress_incr = 100 / self.total_steps
        self.progress = 0
        self.progress_bar.setValue(self.progress)

    def incr_progress(self):
        '''Increments progress bar by displayed value'''
        if not hasattr(self, 'progress'):
            # This is called for manual sync
            # Since sub is already provided, one step less than full sync
            self.init_progress(nsteps=6)
        self.progress += self.progress_incr
        self.progress_bar.setValue(self.progress)

    def find_and_sync(self):
        self.status_label.setText("Retrieving subtitle...")
        self.init_progress(nsteps=7)
        res = self.find_subtitle()

        self.incr_progress()

        if not res:
            self.status_label.setText("Could not find a subtitle for this file.")
            return

        self.sync()

    def find_subtitle(self):
        if self.media_guessed_info is None:
            self.status_label.setText("Please select a video file first.")
            return

        downloaded_sub_path = os.path.join(self.temp_storage.name, str(uuid.uuid4()) + '.srt')
        active_lang = self.lang_dropdown.currentText()

        sub = fetch_sub_if_found(
            self.media_guessed_info,
            active_lang,
            save_to=downloaded_sub_path
        )

        if sub is True:
            print('Subtitle downloaded at: ', downloaded_sub_path)
            self.sub = downloaded_sub_path
            return True

    def load_sub(self):
        text, ok = QFileDialog.getOpenFileName(self, 'Open subtitle file', self.active_path)
        if text is None or text is "":
            return
        # Retain file dialog location for next time
        self.active_path = os.path.dirname(text)

        print('Sub file: {}'.format(text))
        self.sub = text
        filename = os.path.basename(self.sub)
        self.selected_sub_label.setText(filename)

    def sync(self):
        if self.movie is None or self.movie is "":
            self.status_label.setText("Please select a video file first.")
            return

        if self.sub is None or self.sub is "":
            self.status_label.setText("Please select a subtitle first.")

        self.status_label.setText("Initializing sync...")

        filename, ext = os.path.splitext(self.movie)

        save_to = '{}_{}_{}.srt'.format(
            filename,
            self.lang_dropdown.currentText(),
            'captionpal'
        )

        if self.sub is not None and self.movie is not None:
            self.incr_progress()
            thr = Thread(
                target=sync_in_thread,
                args=[self.movie, self.sub, self.onSyncStatusUpdate],
                kwargs={'save_to': save_to}
            )
            thr.start()

    def onSyncStatusUpdate(self, ev):
        self.incr_progress()
        self.status_label.setText(ev)

    def center(self):
        '''Centers the window on the desktop
        '''
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def mousePressEvent(self, event):
        '''Start a drag on the window
        '''
        self.drag_old_pos = event.globalPos()

    def mouseMoveEvent(self, event):
        '''Perform a drag on the window
        '''
        try:
            getattr(self, 'drag_old_pos')
        except:
            # Did not click on a blank area
            return

        if self.drag_old_pos is None:
            # Did not click on a blank area
            return

        delta = QPoint (event.globalPos() - self.drag_old_pos)
        self.move(self.x() + delta.x(), self.y() + delta.y())
        self.drag_old_pos = event.globalPos()

    def mouseReleaseEvent(self, event):
        '''End the drag on the window
        '''
        self.drag_old_pos = None

    def mouseDoubleClickEvent(self, event):
        '''Switch between full screen and resized mode
        '''
        if self.fullScreen is True:
            self.showNormal()
            self.fullScreen = False
        else:
            self.showFullScreen()
            self.fullScreen = True

    def link(self, url):
        print('Opening url: ', url)
        QDesktopServices.openUrl(QUrl(url))

def main_ui():
    print('Building UI')
    app = QApplication(sys.argv)
    print('Processing events')
    app.processEvents()

    app.setStyle('Fusion')

    palette = QPalette()
    palette.setColor(QPalette.Window, QColor(53,53,53))
    palette.setColor(QPalette.WindowText, Qt.white)
    palette.setColor(QPalette.Base, QColor(15,15,15))
    palette.setColor(QPalette.AlternateBase, QColor(53,53,53))
    palette.setColor(QPalette.ToolTipBase, Qt.white)
    palette.setColor(QPalette.ToolTipText, Qt.white)
    palette.setColor(QPalette.Text, Qt.white)
    palette.setColor(QPalette.Button, QColor(53,53,53))
    palette.setColor(QPalette.ButtonText, Qt.white)
    palette.setColor(QPalette.BrightText, Qt.red)
    palette.setColor(QPalette.Highlight, QColor(142,45,197).lighter())
    palette.setColor(QPalette.HighlightedText, Qt.black)
    palette.setColor(QPalette.Link, Qt.white)
    app.setPalette(palette)

    mainWindow = MainWindow()
    mainWindow.setWindowTitle('Autosync')
    ico_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'logo.ico')
    print('Path:', ico_path)
    mainWindow.setWindowIcon(QIcon(ico_path))
    mainWindow.show()

    sys.exit(app.exec_())

def main_cli():
    print('Running CLI')
    # Lazy import of TF
    from autosync.sync import sync
    settings = parse_args()
    print(settings)

    # Check video is found
    if not os.path.exists(settings['video']):
        raise OSError("Video not found on disk.")

    if settings['sub'] is not None and not os.path.exists(settings['sub']):
        raise OSError("Subtitle not found on disk.")

    tempdir = tempfile.TemporaryDirectory()

    # If subtitle is None, retrieve it
    if settings['sub'] is None:
        # Identify video information
        guessed_info = guessit(settings['video'])
        print('Guessed information: {}'.format(guessed_info))
        settings['sub'] = os.path.join(tempdir.name, "sub.srt")
        res = fetch_sub_if_found(guessed_info, settings['lang'], save_to=settings['sub'])
        if not res:
            print('Subtitle not found.')
            return

    # Build final sub name so that it's automatically picked up by media players
    filename, ext = os.path.splitext(settings['video'])
    sub_name = '{}_{}_{}.srt'.format(
        filename,
        settings['lang'],
        'synced'
    )

    # Synchronize
    for step in sync(settings['video'], settings['sub'], save_to=sub_name):
        print(step)

def main_fn():
    logging.basicConfig(level=logging.DEBUG)

    if len(sys.argv) > 1:
        print('Running CLI')
        main_cli()
    else:
        print('Running UI')
        main_ui()

if __name__ == '__main__':
    freeze_support()
    main_fn()
