from glob import glob
import os
import json
import tensorflow as tf
from pathlib import Path
import pysrt
import numpy as np
from moviepy.audio.io.AudioFileClip import AudioFileClip
import matplotlib.pyplot as plt
from scipy.io import wavfile
from scipy.fftpack import dct
from .utils import *
from sklearn.utils import shuffle
import tempfile
import librosa

def list_files(dirlist, globbing=True):
    '''Detects all candidate files matching dirlist, either using a glob pattern
    (with globbing == true) or a list of filenames (using globbing == false)

    Returns: list of paths
    '''
    # List all images
    if not isinstance(dirlist, list):
        dirlist = [dirlist]
    files = []
    for d in dirlist:
        if globbing:
            files += glob(d)
        else:
            if os.path.isfile(d):
                files.append(d)
    return files

def fetch_dataset_slices(dir, test_percent=10, block_ms=32, framerate=16000,
    store_wav_file=True, globbing=True):
    '''
    Attributes:
        dir             Directory where the movies + subtitles are located
        test_percent    Percentage of the data to pick as test data
        block_ms        Duration in ms of each individual audio block
        framerate       Framerate in frames per second
    '''
    # TODO: Extract framerate instead of hardcoding fps
    assert test_percent < 100

    files = list_files(dir)

    if len(files) == 0:
        raise IndexError('No files found under glob {}'.format(dir))

    # Ensure array have an integer amount of blocks
    # Make blocks of 20 ms
    block = int(block_ms * framerate / 1000)
    print('Each block ({} ms) contains {} samples'.format(block_ms, block))

    # Open video and extract audio (not optimized for a large amount of audio but should be enough)
    audioclips = [load_audio(f, block_len=block, store_wav_file=store_wav_file) for f in files]

    # Compute MFCC and spectrogram
    # Only use left channel for now
    mfcc_feats = []
    for i, audioclip in enumerate(audioclips):
        print('Computing MFCC features for video {}...'.format(i))
        feat = librosa.feature.mfcc(y=audioclip, sr=framerate)
        feat = feat.transpose((1, 0))
        '''
        feat = logfbank(
            audioclip[:,0],
            samplerate=framerate,
            winlen=block_ms/1000.0,
            winstep=block_ms/1000.0
        )
        '''
        # Only keep filters 2 to 13
        #feat = feat[:, 1:14]
        #feat = feat[:, :16]
        #feat = dct(feat)
        feat = feat[:, :14]
        # Remap to approx. 0 - 1
        #feat = feat / 1000.0 + 1.0
        print('Feats min/max: {}/{}'.format(
            np.amin(feat),
            np.amax(feat)
        ))
        feat = feat.astype(np.float32)
        feat = np.expand_dims(feat, axis=-1)
        mfcc_feats.append(feat)

    #play_audio(audioclips[0])
    if test_percent > 0:
        # Find associated subtitles and sequences where people speak
        subs = [os.path.splitext(f)[0] + '.srt' for f in files]
        sequences = [sub_to_sequence(s, fps=framerate, total_length=audioclips[i].shape[0]) for i, s in enumerate(subs)]

        #xaxis = np.arange(0, sequences[0].shape[0], 1)
        #xaxis = xaxis / float(framerate)
        #plt.plot(xaxis, sequences[0])
        #plt.show()

        # Blocks of subtitles
        subtitles_blocks = [s.reshape((s.shape[0] // block, block)) for s in sequences]
        subtitles_blocks = [contains_speech(s, block_ms) for s in subtitles_blocks]

        #sb = subtitles_blocks[0][:, 0]
        #sb[sb < 1] = 0

        #sb[sb > 0] = 1
        #sb = 1 + (sb * -1)

        #ab = audioclips[0]
        #reconstruct_sub(sb, ab, block_len_ms=block_ms)
        #import pdb; pdb.set_trace()

        mfcc_feats_normed = []
        subtitles_blocks_normed = []
        for audio, label in zip(mfcc_feats, subtitles_blocks):
            if audio.shape[0] != label.shape[0]:
                print('Audio {} vs Label {} 1st dim dont match. Fixing it'.format(
                    audio.shape,
                    label.shape
                ))
                mini = min(audio.shape[0], label.shape[0])
                audio = audio[:mini]
                label = label[:mini]

            mfcc_feats_normed.append(audio)
            subtitles_blocks_normed.append(label)
        mfcc_feats = mfcc_feats_normed

        # labels contains information about whether there is someone speaking or not at each datapoint
        # audios contains the actual audio information
        label_blocks = np.concatenate(subtitles_blocks_normed, axis=0)

    feats = np.concatenate(mfcc_feats, axis=0)

    if test_percent == 0:
        return feats
    else:
        # Overfitting
        start = 0
        span = 300000

        print('Feature count before filtering: ', feats.shape[0])

        assert feats.shape[0] > start + span
        feats = feats[start:start+span]
        label_blocks = label_blocks[start:start+span]

        feats, label_blocks = shuffle(feats, label_blocks)
        feats, label_blocks = take_01(feats, label_blocks)

        # Split into train and test dataset
        test_start = int(label_blocks.shape[0] * test_percent / 100.0)
        assert test_start > 0
        train_set = feats[:-test_start]
        train_label = label_blocks[:-test_start]
        test_set = feats[-test_start:]
        test_label = label_blocks[-test_start:]

        return (train_set, train_label), (test_set, test_label)

def ensure_multiple(ar, block_len):
    mod = ar.shape[0] % block_len
    mod = block_len - mod
    arr = np.pad(ar, (0, mod), 'constant')
    assert arr.shape[0] / block_len == arr.shape[0] // block_len
    return arr

def take_01(feats, labels):
    zero_ones = (labels == 0) | (labels == 1)
    zero_ones = zero_ones[:, 0]
    labels = np.compress(zero_ones, labels, axis=0)
    feats = np.compress(zero_ones, feats, axis=0)
    return feats, labels

def group_samples(audio, labels, len=4):
    count, freqs, _ = audio.shape

    audio = audio.reshape((count // len, len, freqs, _))
    labels = labels.reshape((count // len, -1))
    labels = np.sum(labels, axis=-1, keepdims=True) / len

    return audio, labels

def contains_speech(labels, block):
    '''Detects if each block entirely contains speech, partially, or not at all.
    Return 0 for not at all
    1 for speech at all
    0.5 otherwise
    '''
    npoints, ndim = labels.shape
    labels = np.sum(labels, axis=-1)
    #slc_fullspeech = labels == ndim
    #flags = np.zeros(labels.shape)
    #flags[slc_fullspeech] = 1
    flags = labels.astype(np.float32)
    flags = flags / ndim
    flags = np.expand_dims(flags, axis=-1)
    return flags

def load_audio(filename, fps=16000, block_len=1, store_wav_file=False):
    print('Loading {}'.format(filename))

    base, ext = os.path.splitext(filename)
    processed_file = base + '.wav'

    tempdir = tempfile.TemporaryDirectory()

    # If no .wav file is found, extract audio from video
    if not os.path.isfile(processed_file):
        audioclip = AudioFileClip(filename)

        if store_wav_file:
            print('Saving audio wav permanently to: {}'.format(processed_file))
        else:
            processed_file = os.path.basename(processed_file)
            processed_file = os.path.join(tempdir.name, processed_file)
            print('Saving audio to temp file: {}'.format(processed_file))

        audioclip.write_audiofile(processed_file, fps=fps)

    #fs, arr = wavfile.read(processed_file)
    arr, sr = librosa.load(processed_file, sr=fps, mono=True)
    assert sr == fps, 'Loaded SR: {} vs FPS: {}'.format(sr, fps)

    arr = ensure_multiple(arr, block_len)

    return arr

def ignore_sequence(sentence):
    '''Detects hearing-impaired sentence such as `(noise)`, music sequences and
    various indications written by subbers that should not be considered as
    speech during training and synchronization.

    Attributes:
        sentence        A subtitle sentence
    Returns:
        True if regular sentence, false if hearing-impaired indication
    '''
    # Short sequence detection
    if len(sentence) < 3:
        return True

    # Hearing-impaired sequence detection
    chars = [
        ('[', ']'),
        ('(', ')'),
    ]
    surrounded = [sentence[0] == start and sentence[-1] == end for (start,end) in chars]
    if any(surrounded):
        return True

    # Music sequence detection
    if '♪' in sentence:
        return True

    # If there's addic7ed in the sub, it's most likely not speech
    if 'addic7ed' in sentence:
        return True

    return False

def sub_to_sequence(sub, total_length=None, fps=16000,
    ignore_fn=ignore_sequence, dump_ms=400):
    '''
    Loads a subtitle file, converts it to an array filled with 0s and 1s.
    0s correspond to the lack of speech.
    Subtitles containing characters provided in filter will be ignored.

    Attributes:
        sub             Subtitle file
        total_length    Length of the file in datapoints ? TODO: Remove this
        fps             Audio framerate to use for building the 0-1 array
        ignore_fn       Function to filter sentences in a subtitle
        dump_ms         A delay at the beginning/end of each sentence to mark
            as .5 instead of 0/1 (beginning and end of sentences are often not
            well aligned with speech and may start earlier or end later)
    '''
    print('Loading sub: {}'.format(sub))
    subs = pysrt.open(sub)
    end_time = pysrt_date_to_milliseconds(subs[-1].end) * fps / 1000

    sequence = np.zeros((total_length), dtype=np.float32)

    def tb(x):
        return int(pysrt_date_to_milliseconds(x) * fps / 1000)

    dump = int(dump_ms * fps / 1000)
    print('Dumping {} points at start/end'.format(dump))

    for sentence in subs:
        if ignore_fn(sentence.text):
            continue

        start = tb(sentence.start)
        end = tb(sentence.end)

        if end > total_length:
            "Sub is beyond EOF: {} vs {}. Ignoring".format(end, total_length)
            continue

        #print(start, end, dump)
        sequence[start+dump:end-dump] = 1

        #plt.plot(sequence[:end + 20000])

        # Mark beginning and end of subs as .5 so they're discarded afterwards
        sequence[start-dump:start+dump] = 0.5
        #import pdb; pdb.set_trace()

        #plt.plot(sequence[:end + 20000])

        sequence[end-dump:end+dump] = 0.5

        #plt.plot(sequence[:end + 20000])
        #plt.show()


    return sequence

def resync_sub(filename, offset=0, ratio=1):
    subs = pysrt.open(filename)
    subs.shift(ratio=ratio)
    subs.shift(seconds=offset)
    return subs

def pysrt_date_to_milliseconds(d):
    total =  d.hours * 3600 * 1000
    total += d.minutes * 60 * 1000
    total += d.seconds * 1000
    total += d.milliseconds

    return total

def parse_function(audio, label):
    a = tf.convert_to_tensor(audio, dtype=audio.dtype)
    return a, label

def create_dataset(audios, labels):
    '''Creates the TF dataset'''
    dataset = tf.data.Dataset.from_tensor_slices((audios, labels))
    dataset = dataset.shuffle(len(audios))
    #dataset = dataset.map(parse_function, num_parallel_calls=4)
    #dataset = dataset.map(train_preprocess, num_parallel_calls=4)
    return dataset
