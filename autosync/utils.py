import pysrt
from itertools import accumulate
import numpy as np
from scipy.io import wavfile

def contiguous_regions(condition):
    # From https://stackoverflow.com/a/4495197/4218017
    """Finds contiguous True regions of the boolean array "condition". Returns
    a 2D array where the first column is the start index of the region and the
    second column is the end index."""

    # Find the indicies of changes in "condition"
    d = np.diff(condition)
    idx, = d.nonzero()

    # We need to start things after the change in "condition". Therefore,
    # we'll shift the index by 1 to the right.
    idx += 1

    if condition[0]:
        # If the start of condition is True prepend a 0
        idx = np.r_[0, idx]

    if condition[-1]:
        # If the end of condition is True, append the length of the array
        idx = np.r_[idx, condition.size] # Edit

    # Reshape the result into two columns
    idx.shape = (-1,2)
    return idx

def f_delay(s):
    # hours
    hours = s // 3600
    # remaining seconds
    s = s - (hours * 3600)
    # minutes
    minutes = s // 60
    # remaining seconds
    seconds = s - (minutes * 60)
    # total time
    milliseconds = int((seconds - int(seconds)) * 100)
    return '{:02}:{:02}:{:02},{:03}'.format(int(hours), int(minutes), int(seconds), milliseconds)

def export_audio(arr, audio, block_len_ms=20):
    assert arr.ndim == 1

    for i, (start, end) in enumerate(contiguous_regions(arr)):
        start_s = f_delay(start * block_len_ms / 1000.0)
        end_s = f_delay(end * block_len_ms / 1000.0)

        dat = audio[start * 512:end * 512]
        wavfile.write('out/s{}.wav'.format(i), 16000, dat)
        print('i: {} | start: {} ({} s) | end: {} ({} s)'.format(i, start, start_s, end, end_s))

def reconstruct_sub(arr, block_len_ms=20):
    sub = ""

    tmpl ="{index}\n{start} --> {end}\n<i>SPEAKING</i>\n\n"
    assert arr.ndim == 1

    for i, (start, end) in enumerate(contiguous_regions(arr)):
        start_s = f_delay(start * block_len_ms / 1000.0)
        end_s = f_delay(end * block_len_ms / 1000.0)
        f = tmpl.format(index=i+1, start=start_s, end=end_s)
        sub += f
        #print('Start {} end {}'.format(start, end))

    with open('sub_got.srt', 'w+') as f:
        f.write(sub)

    print('Sub saved')
