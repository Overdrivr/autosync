import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt
from scipy.signal import medfilt

from autosync.dataset import *
from autosync.utils import *

def rolling_avg(iterable):
    cumu_sum = accumulate(iterable)
    yield from (accu/i for i, accu in enumerate(cumu_sum, 1))

def verify(dir):
    test_audio = fetch_dataset_slices(dir, test_percent=0)

    graph = tf.Graph()
    with graph.as_default():
        with tf.Session() as sess:
            meta_graph_def = tf.saved_model.loader.load(
                sess,
                [tf.saved_model.tag_constants.SERVING],
                'saved_models/m17_dataclean_300k_balanced',
            )
            signature = meta_graph_def.signature_def
            signature_key = tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY
            print('Model signature:', signature)
            x_tensor_name = signature[signature_key].inputs['audio_mfcc'].name
            y_tensor_name = signature[signature_key].outputs['has_speech'].name

            x = sess.graph.get_tensor_by_name(x_tensor_name)
            y = sess.graph.get_tensor_by_name(y_tensor_name)

            result = sess.run(y, feed_dict={
                x: test_audio,
            })


    #result = model.predict(
    #    test_audio,
    #)

    #result[result > 0.3] = 1.0
    #result[result < 1.0] = 0.0


    xaxis = np.arange(0, result.shape[0], 1)
    xaxis = xaxis * 0.032 # float(16000)
    '''
    def smooth(y, box_pts):
        box = np.ones(box_pts)/box_pts
        y_smooth = np.convolve(y, box, mode='same')
        return y_smooth
    '''

    result_bin = np.zeros(result[:, 0].shape)
    result_bin[result[:, 0] > 0.5] = 1

    result_bin_f = medfilt(result_bin, kernel_size=21)
    reconstruct_sub(result_bin_f, block_len_ms=32)

    plt.plot(xaxis, result[:, 0])
    plt.plot(xaxis, result_bin)
    plt.plot(xaxis, result_bin_f)
    plt.show()

    #np.save('result.npy', result)
    #result = np.load('result.npy')
    #import pdb; pdb.set_trace()

    print('Result: {}'.format(result.shape))

def main():
    #dir = "datasets/101*.avi"
    #dir = "datasets/Game.Of.Thrones.S02E02*.mkv"
    dir = "C:/Users/remib/Downloads/Game.of.Thrones.Season.6.720p.BluRay.x264.ShAaNiG/Game.of.Thrones.S06E04.720p.BluRay.x264.ShAaNiG.mkv"
    verify(dir)

if __name__ == "__main__":
    main()
