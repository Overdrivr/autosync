[Setup]
AppName=CaptionPal
AppVersion={{ VERSION }}
AppCopyright=Copyright (C) 2019 Rémi Bèges
DefaultDirName={pf}\CaptionPal
OutputDir=output
OutputBaseFilename={{ PLATFORM }}
AppPublisher=Rémi Bèges
AppPublisherURL=https://captionpal.org/
ArchitecturesInstallIn64BitMode=x64
ArchitecturesAllowed=x64
ChangesEnvironment=true
DisableWelcomePage=no
DisableDirPage=no
LicenseFile=LICENSE

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; \
    GroupDescription: "{cm:AdditionalIcons}"

[Files]
Source: "dist\captionpal\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs
Source: "LICENSE"; DestDir: "{app}"

[Icons]
Name: "{userdesktop}\CaptionPal"; Filename: "{app}\CaptionPal.exe"; \
    IconFilename: "{app}\logo.ico"; Tasks: desktopicon
