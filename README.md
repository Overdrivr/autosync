# CaptionPal - Automatic subtitle download and synchronization using machine learning

CaptionPal solves the annoying issue of finding and synchronizing subtitles for TV series and movies. ([Project page](https://captionpal.org)).

It uses a speech detection model that was trained on a few hours of TV series.
This model is capable to tell when in an audio track there's human speech.

CaptionPal then synchronizes the subtitle with this detected human speech track.
Subtitles are downloaded automatically from various sources (addic7ed.com for now), just from the video's filename.

Runs on Windows and Unix (MacOS soon).

## Downloads

Pre-built binaries can be found on the project page at https://captionpal.org.

## Install

```
pipenv install --dev
```

## Run (UI)

Calling `autosync-ui` without arguments launches the interface.

```
pipenv run autosync-ui
```

## Run (CLI)
### From source

Caling `autosync-ui` with arguments launches the command-line.

```
pipenv run autosync-ui --help
pipenv run autosync-ui <video filename> --lang spanish
```

## Detect speech

It is also possible to output a `.srt` file from a video alone, that contains `SPEAKING` sequences where CaptionPal has detected human speech. It is useful to assessing the quality of the generated model.

```
pipenv run autosync-verify
```

## Train speech detection model

If you wish to retrain the speech detection model, create a datasets folder here in this repository, and put various video's with their matching subtitles.

```
/autosync
/datasets
  | FavoriteTvSeries 1x01.avi
  | FavoriteTvSeries 1x01.srt
  | FavoriteTvSeries 1x02.avi
  | FavoriteTvSeries 1x02.srt
/test
```

Then, launch training with:

```
pipenv run autosync-train
```

You can change the model's ID to save & compare multiple models, by modifying `model_id` in `autosync/train.py`

Also, in another console, feel free to launch tensorboard to observe training progress.

```
pipenv run tensorboard --logdir=logs/
```

## A word of caution

Beware that, as with any ML-based approach, clean data is more than fundamental. Ensure your reference subtitles are well synchronized. Less data but of better quality is preferable to more bad data.

Also, **pay attention to dataset imbalance** for train and test dataset. Your dataset should contain ~50% of blocks with speech (and so 50% of blocks without speech).

Dataset balancing is displayed at the beginning of the training:

```
Training dataset balance: 51%
Test dataset balance: 48%
```

Value needs to be close to 50% otherwise the loss and accuracy as written in the training model don't have any sense.

Imagine your dataset is imbalanced 20%/80%, you can reach a very good value for accuracy metric, but it may just mean your model outputs `1s` more often than `0s`, and is not actually learning anything meaningful.

In practice, TV comedy shows have high speech ratios (70%-80%) because characters speak quickly and often.

TV Dramas are slower, and likely below 50%.

Therefore, a good balance can be found by combining comedy and drama.


# Literature & Reference

Heavily inspired by:

- https://machinelearnings.co/automatic-subtitle-synchronization-e188a9275617

# License

This software is released under the GNU General Public License v3.0.
See LICENSE file in this repository.
