#!/usr/bin/env python
from setuptools import setup, find_packages

# Load version
exec(open("./autosync/config.py").read())

setup(
    name='autosync',
    version=__version__,
    description='Sync subs automatically with AI',
    author='Rémi Bèges',
    author_email='remi.beges@gmail.com',
    url='',
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'autosync-ui=autosync.ui:main_fn',
            'autosync-train=autosync.train:main',
            'autosync-verify=autosync.verify:main',
            'autosync-sync=autosync.sync:main',
        ]
    },
    install_requires=[
    ],
    dependency_links=[
    ],
)
