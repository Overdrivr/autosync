1
00:00:03,371 --> 00:00:05,605
Hello. Hi.

2
00:00:06,346 --> 00:00:10,154
My name is Leslie Knope, and I work for
the Parks and Recreation Department.

3
00:00:10,225 --> 00:00:12,459
Can I ask you a few questions?

4
00:00:13,501 --> 00:00:15,199
Would you say that you are,

5
00:00:15,273 --> 00:00:17,438
"Enjoying yourself
and having fun,

6
00:00:17,513 --> 00:00:19,381
"having a moderate
amount of fun

7
00:00:19,452 --> 00:00:20,887
"and somewhat
enjoying yourself,

8
00:00:20,955 --> 00:00:23,657
"or having no fun
and no enjoyment?"

9
00:00:25,135 --> 00:00:26,935
I'm gonna put a lot of fun.

10
00:00:27,007 --> 00:00:30,210
Ms. Knope,
there's a drunk stuck in the slide.

11
00:00:30,709 --> 00:00:32,638
Sir, this is
a children's slide.

12
00:00:32,668 --> 00:00:34,242
- You're not allowed to sleep in here.
- What is?

13
00:00:34,267 --> 00:00:35,388
<i>You know,</i>
<i>when I first tell people</i>

14
00:00:35,413 --> 00:00:36,760
<i>that I work in the government,</i>
<i>they say,</i>

15
00:00:36,791 --> 00:00:38,626
"Oh. The government.
The government stinks.

16
00:00:38,674 --> 00:00:40,759
"The lines are too long
at the DMV."

17
00:00:40,799 --> 00:00:42,429
<i>But now things have changed.</i>

18
00:00:42,485 --> 00:00:44,149
<i>People need our help.</i>

19
00:00:44,224 --> 00:00:46,527
And it feels good to be needed.

20
00:00:46,598 --> 00:00:48,101
Could you put
your arms to your side?

21
00:00:48,135 --> 00:00:48,840
Wait...

22
00:00:48,867 --> 00:00:50,515
And that might help you
slide down a little easier.

23
00:00:50,548 --> 00:00:51,919
- We don't...
- Do you want to come this way?

24
00:00:51,938 --> 00:00:53,374
- Uh-uh.
- Okay, we're gonna need you to get out.

25
00:00:53,414 --> 00:00:55,602
Can you please go away?
Get out of the slide. Okay?

26
00:00:55,625 --> 00:00:57,323
You're... Can you please go away?
Here we go!

27
00:00:57,396 --> 00:00:59,561
Okay, wake up. Here we go.
Out of the slide.

28
00:00:59,636 --> 00:01:02,109
<i>You know, government</i>
<i>isn't just a boys' club anymore.</i>

29
00:01:02,176 --> 00:01:07,180
Women are everywhere. It's a great
time to be a woman in politics.

30
00:01:07,258 --> 00:01:11,065
<i>Hillary Clinton,</i>
<i>Sarah Palin, me, Nancy Pelosi.</i>

31
00:01:11,404 --> 00:01:12,738
We did it!

32
00:01:13,108 --> 00:01:14,909
<i>You know, I like to</i>
<i>tell people, you know,</i>

33
00:01:14,981 --> 00:01:16,713
<i>"Get on board and buckle up,</i>

34
00:01:16,786 --> 00:01:19,123
<i>"because my ride's</i>
<i>gonna be a big one."</i>

35
00:01:19,193 --> 00:01:20,823
<i>And if you get motion sickness,</i>
<i>you know,</i>

36
00:01:20,898 --> 00:01:22,459
<i>put your head</i>
<i>between your knees</i>

37
00:01:22,537 --> 00:01:25,409
'cause Leslie Knope's
stopping for no one.

38
00:01:56,770 --> 00:01:59,642
Tonight is our next monthly

39
00:01:59,711 --> 00:02:01,546
community outreach
public forum.

40
00:02:01,617 --> 00:02:03,418
And that is tonight.

41
00:02:03,824 --> 00:02:05,021
That is tonight.
Right.

44
00:02:09,340 --> 00:02:10,833
Who wants in?

45
00:02:15,792 --> 00:02:17,057
Tom.

46
00:02:17,129 --> 00:02:18,429
Fine.

47
00:02:18,969 --> 00:02:21,032
<i>This is a great thing</i>
<i>for you guys to see.</i>

48
00:02:21,108 --> 00:02:23,205
This is where the rubber of government

49
00:02:23,281 --> 00:02:26,518
meets the road
of actual human beings.

50
00:02:26,958 --> 00:02:29,363
When I go through these doors,
I need to be on.

51
00:02:29,432 --> 00:02:32,372
Like the White House Press Secretary.
Are you ready?

52
00:02:32,441 --> 00:02:33,842
- Yeah.
- Okay.

53
00:02:33,912 --> 00:02:35,405
Here we go.

54
00:02:37,122 --> 00:02:38,523
It's locked.

55
00:02:39,763 --> 00:02:42,133
Okay.
Here we go.

56
00:02:49,424 --> 00:02:52,889
<i>Thank you so much for coming.</i>
<i>What an amazing turnout.</i>

57
00:02:53,436 --> 00:02:54,530
<i>My name is Leslie Knope.</i>

58
00:02:54,606 --> 00:02:57,844
<i>I am the deputy director of the</i>
<i>Parks and Recreation Department,</i>

59
00:02:57,916 --> 00:03:02,327
<i>and tonight we're gonna be taking</i>
<i>some of your questions as... Hello?</i>

60
00:03:03,633 --> 00:03:05,935
<i>We're having a meeting in here.</i>

61
00:03:09,516 --> 00:03:11,751
<i>Does anybody have any</i>
<i>questions about permits?</i>

62
00:03:12,425 --> 00:03:14,659
So, take two.

63
00:03:14,731 --> 00:03:18,539
I'm Leslie Knope, and with me is
department member Tom Haverford.

64
00:03:18,610 --> 00:03:22,314
We are here to answer any and all
of your questions, so fire away.

65
00:03:22,387 --> 00:03:26,422
Well, it's a great day,
because last month they put me in jail.

66
00:03:26,499 --> 00:03:32,312
That's right. The head of the
police is a ninth-degree Mason.

67
00:03:32,383 --> 00:03:34,879
But the music is so loud.

68
00:03:34,957 --> 00:03:37,430
Stop the graffiti,
please. Please.

69
00:03:37,498 --> 00:03:39,801
I don't like obscenities
just as much as you don't like them.

70
00:03:39,872 --> 00:03:41,206
No, it drives me
crazy. I have kids.

71
00:03:41,276 --> 00:03:42,302
Right. But...

72
00:03:42,379 --> 00:03:43,643
I've got my little
three-year-old,

73
00:03:43,716 --> 00:03:45,243
I'm going through the
park and someone's like,

74
00:03:45,321 --> 00:03:46,621
"Hey my"

75
00:03:46,692 --> 00:03:48,961
And the guy's, "You my,
you head."

76
00:03:49,032 --> 00:03:50,730
These people are
members of a community

77
00:03:50,804 --> 00:03:52,399
that care about
where they live,

78
00:03:52,475 --> 00:03:55,815
<i>so what I hear when I'm being yelled at</i>

79
00:03:55,886 --> 00:03:58,793
is people caring loudly at me.

80
00:03:59,329 --> 00:04:02,566
Now, I have a few things I
want to say about Laura Linney.

81
00:04:02,639 --> 00:04:05,238
Thank you so much, Barry.
Always great to have you here.

82
00:04:05,313 --> 00:04:07,045
Anyone else who would
like to contribute?

83
00:04:07,119 --> 00:04:08,145
Hi. Hi, hi.

84
00:04:08,222 --> 00:04:09,555
Hi. I'm Ann Perkins.

85
00:04:09,625 --> 00:04:15,006
I'm a nurse, and frankly,
I don't really care for politics,

86
00:04:21,460 --> 00:04:25,199
but I'm here to talk about the
abandoned lot on Sullivan Street.

87
00:04:25,234 --> 00:04:27,163
Excellent. That sounds like a good idea.
Tell us about that.

88
00:04:27,193 --> 00:04:30,259
No, it's a problem.
It almost killed my boyfriend.

89
00:04:30,286 --> 00:04:31,380
- Oh.
- Yeah.

90
00:04:31,457 --> 00:04:33,360
There's a lot nearby my house,

91
00:04:33,428 --> 00:04:36,632
and a developer dug out a
basement for some condos,

92
00:04:36,705 --> 00:04:38,198
and then they went bankrupt,

93
00:04:38,277 --> 00:04:41,252
so there's just this giant pit,

94
00:04:41,319 --> 00:04:43,153
and it's been there
for almost a year.

95
00:04:43,224 --> 00:04:44,855
Twelve months,
yes. Go on.

96
00:04:44,930 --> 00:04:49,740
Yeah, and my boyfriend, who is a
musician, actually, I support him,

97
00:04:49,810 --> 00:04:53,446
but anyway, he fell in
and broke both his legs.

98
00:04:53,521 --> 00:04:54,821
Ann, let me speak
with you for a minute.

99
00:04:54,892 --> 00:04:57,799
So, your boyfriend fell
down into this pit, right?

100
00:04:57,867 --> 00:04:58,893
Yes.

101
00:04:58,971 --> 00:05:03,679
And, this guy, is it pretty serious?
Are you guys living together?

102
00:05:03,751 --> 00:05:05,449
- Yes.
- Wow,

103
00:05:05,522 --> 00:05:06,958
I'm sure this must be
really tough for you.

104
00:05:07,027 --> 00:05:10,401
You know, just, this guy, sounds like
he didn't have a lot going on for him

105
00:05:10,471 --> 00:05:12,739
to start with, and now
both legs broken.

106
00:05:12,811 --> 00:05:15,376
He's just weak.
You have to take care of him.

107
00:05:15,452 --> 00:05:17,013
You probably feel like
you need to move on.

108
00:05:17,090 --> 00:05:18,994
What does this...
Just become more adventurous

109
00:05:19,063 --> 00:05:20,693
in relationships
with your body, just in...

110
00:05:20,768 --> 00:05:24,039
Are you actually
hitting on me right now?

111
00:05:24,110 --> 00:05:26,914
Oh. Oh. God, no. I'm not hitting on you.
I'm actually married.

112
00:05:26,986 --> 00:05:30,291
I'm just an open person,
and I like connecting with people.

113
00:05:30,362 --> 00:05:33,269
I'm, you know, very comfortable
around women, attractive women.

114
00:05:33,337 --> 00:05:36,471
I've spent a lot of time with them.
And I just... I don't know.

115
00:05:36,547 --> 00:05:38,576
I feel like we might be taking
up too much of the forum's time.

116
00:05:38,653 --> 00:05:41,253
Maybe we can just
exchange numbers.

117
00:05:41,328 --> 00:05:44,531
You know, maybe go away one weekend
and just kind of talk about this.

118
00:05:44,604 --> 00:05:46,199
- Look...
- I'd love to do it.

119
00:05:46,275 --> 00:05:50,549
The bottom line is I've been trying
to get this thing fixed for months,

120
00:05:50,622 --> 00:05:53,391
and nobody's done anything,
and it's ugly, and it's dangerous,

121
00:05:53,463 --> 00:05:54,933
and it's
government-owned,

122
00:05:55,001 --> 00:05:57,064
and you need to do
something about it.

123
00:06:02,857 --> 00:06:04,020
Okay.

124
00:06:05,365 --> 00:06:07,132
- I'll do something about it.
- Really?

125
00:06:07,204 --> 00:06:09,301
Yes, we...
I will help you.

126
00:06:09,377 --> 00:06:11,315
Is that a promise?

127
00:06:12,251 --> 00:06:15,455
It's more than a promise.
It's a pinky promise.

128
00:06:16,565 --> 00:06:20,531
I pinky promise all of
you that I will help,

129
00:06:21,780 --> 00:06:24,447
and I will go to
that location tomorrow,

130
00:06:24,521 --> 00:06:26,720
and we will fill in that pit,

131
00:06:26,795 --> 00:06:31,935
and then when that's done,
we're gonna put a park on the land.

132
00:06:35,018 --> 00:06:36,009
Okay.

133
00:06:40,601 --> 00:06:42,402
<i>Well, I've worked at the</i>
<i>Parks Department for six years,</i>

134
00:06:42,474 --> 00:06:44,639
<i>and I've handled a lot of</i>
<i>things that I'm proud of.</i>

135
00:06:44,713 --> 00:06:46,776
Recently, I led
a city-wide drive

136
00:06:46,853 --> 00:06:48,518
to disinfect the sandbox sand

137
00:06:48,592 --> 00:06:51,464
after we had those
problems with the cats.

138
00:06:52,101 --> 00:06:54,005
<i>But this pit,</i>

139
00:06:54,074 --> 00:06:57,277
the chance to build a whole
new park from scratch,

140
00:06:59,056 --> 00:07:01,255
this could be my Hoover Dam.

141
00:07:05,705 --> 00:07:06,969
No, that sounds great.

142
00:07:07,042 --> 00:07:10,952
Problem is,
anything over $25 I have to report,

143
00:07:11,020 --> 00:07:15,466
so maybe give my wife a call
and give her the suits,

144
00:07:15,534 --> 00:07:18,303
and then if they don't fit her,
maybe she'll give them to me.

145
00:07:19,545 --> 00:07:20,810
Morning.

146
00:07:21,886 --> 00:07:24,861
All right,
Mr. Mayor. That sounds good.

147
00:07:24,928 --> 00:07:27,731
I'll see you on Saturday for
the Ultimate Frisbee game.

148
00:07:27,802 --> 00:07:28,793
Bye.

149
00:07:29,441 --> 00:07:31,071
Leslie, hello.

150
00:07:31,146 --> 00:07:32,639
You were talking to the mayor?

151
00:07:32,717 --> 00:07:35,658
Yep, we were just rapping
about some things.

152
00:07:35,726 --> 00:07:39,328
Tom and I work really well together.
We're both outsiders.

153
00:07:39,404 --> 00:07:42,972
I'm a woman, he's a...
I think he's a Libyan.

154
00:07:43,449 --> 00:07:45,683
I am from Bennettsville,
South Carolina.

155
00:07:45,755 --> 00:07:48,194
I am what you might call
a redneck.

156
00:07:48,263 --> 00:07:50,930
Okay, brainstorm.
How do we make this park happen?

157
00:07:51,004 --> 00:07:52,304
Let's go to
the city council directly

158
00:07:52,375 --> 00:07:53,401
and ask them
to put up the money.

159
00:07:53,478 --> 00:07:55,781
No, parks are not a priority.

160
00:07:55,852 --> 00:07:59,089
I need more firepower.
I need bigger guns.

161
00:07:59,161 --> 00:08:01,828
You know what I need to do?
Form a committee. Right?

162
00:08:01,902 --> 00:08:04,033
Yeah, that could work.
Yeah, 'cause committees are power,

163
00:08:04,110 --> 00:08:05,671
and committees
make things happen.

164
00:08:05,748 --> 00:08:09,282
Committees are the lifeblood
of our democratic system.

165
00:08:09,358 --> 00:08:10,885
That's really good.
Write that down.

166
00:08:10,963 --> 00:08:12,091
Yep.

167
00:08:12,166 --> 00:08:15,039
From time to time, when I think
of an eloquent saying or a phrase,

168
00:08:15,108 --> 00:08:16,372
I have Tom write it down.

169
00:08:16,445 --> 00:08:18,076
He's collecting them
for my memoirs.

170
00:08:18,151 --> 00:08:20,784
Here's Leslie's quote
from Wednesday.

171
00:08:21,427 --> 00:08:23,091
Okay, read it back to me.

172
00:08:23,165 --> 00:08:24,430
Um...

173
00:08:25,372 --> 00:08:28,244
"Committees cover our
democracy with blood."

174
00:08:28,313 --> 00:08:29,408
Hmm.

175
00:08:31,389 --> 00:08:33,760
Sounded better when I said it.

176
00:08:33,830 --> 00:08:35,596
It's still good, though.

177
00:08:35,669 --> 00:08:39,635
Okay, I have an idea.
What about bringing Mark on board?

178
00:08:39,713 --> 00:08:41,582
- Which Mark?
- Which Mark?

179
00:08:42,020 --> 00:08:45,018
Mark Brendanawicz.
Which Mark?

180
00:08:45,096 --> 00:08:46,639
Well, if you want something
done in this town,

181
00:08:46,664 --> 00:08:47,854
you call Mark Brendanawicz

182
00:08:47,871 --> 00:08:50,310
because, you know, he's a city planner,
but he's more than that.

183
00:08:50,378 --> 00:08:52,350
He's kind of like a fixer.
He fixes things.

184
00:08:52,417 --> 00:08:56,019
He's a smart, capable guy.

185
00:08:56,095 --> 00:08:59,036
He just... He knows where
the bodies are buried.

186
00:08:59,104 --> 00:09:02,375
What's up, Brendanawicz?
You crazy old Polish person.

187
00:09:03,584 --> 00:09:06,524
<i>City Hall is like a locker room,</i>
<i>and you gotta get in there,</i>

188
00:09:06,592 --> 00:09:09,088
and you gotta snap
towels at people,

189
00:09:09,166 --> 00:09:12,107
and you gotta give them the business,
and if you can't take it,

190
00:09:12,175 --> 00:09:14,374
you know, you...
Then you can't take it.

191
00:09:14,449 --> 00:09:16,887
You gotta leave
the locker room.

192
00:09:17,925 --> 00:09:18,985
Let's get down to brass tacks.

193
00:09:19,062 --> 00:09:20,293
I know you don't
have a lot of time

194
00:09:20,366 --> 00:09:23,535
and I want to thank you
for meeting me today.

195
00:09:23,609 --> 00:09:24,943
I just work right there.

196
00:09:25,013 --> 00:09:26,677
Well, thank you for
clearing your schedule.

197
00:09:26,751 --> 00:09:27,948
I didn't clear anything.

198
00:09:28,022 --> 00:09:29,788
You know this lot that
I'm talking about, right?

199
00:09:29,860 --> 00:09:32,095
Yeah, Lot 48.

200
00:09:32,167 --> 00:09:35,940
Condo developer. He dug a basement,
and then he went bankrupt.

201
00:09:36,012 --> 00:09:39,112
I checked it out like a month ago.
It's pretty gross.

202
00:09:39,187 --> 00:09:41,250
What would you say
if I told you

203
00:09:41,328 --> 00:09:44,735
that I was thinking about
turning it into a park?

204
00:09:44,804 --> 00:09:47,836
A park, huh? Okay.

205
00:09:47,913 --> 00:09:50,010
Well, when I think
about the logistics,

206
00:09:50,086 --> 00:09:52,890
the various hoops that you're
gonna have to jump through,

207
00:09:52,962 --> 00:09:58,468
I would say, is it likely?
No, it's not likely, you know.

208
00:09:58,545 --> 00:10:02,420
But, is it possible?

209
00:10:03,092 --> 00:10:05,496
No. It's not possible.

210
00:10:05,565 --> 00:10:06,625
I would give up on that one.

211
00:10:06,702 --> 00:10:07,898
What? Why?

212
00:10:07,972 --> 00:10:11,311
Why? I don't know.
There's like a million reasons why.

213
00:10:11,383 --> 00:10:13,787
Homeowners<i>'</i> associations,
anti-government nuts,

214
00:10:13,856 --> 00:10:15,794
bureaucrats,
miles of red tape...

215
00:10:15,862 --> 00:10:17,891
Really? It sounds like you're
telling me to go for it.

216
00:10:17,968 --> 00:10:19,404
There's no way it can happen.

217
00:10:19,472 --> 00:10:22,778
I can do this.
I just need a little help.

218
00:10:24,086 --> 00:10:27,928
Could you do me a favor,
for old times<i>'</i> sake?

219
00:10:31,441 --> 00:10:34,575
Mark and I...
It's complicated.

220
00:10:35,286 --> 00:10:39,127
When you work closely with someone
and you share similar interests

221
00:10:39,197 --> 00:10:41,670
<i>and you have</i>
<i>a similar world view</i>

222
00:10:41,737 --> 00:10:45,579
and you're passionate about the
same things, things can happen.

223
00:10:47,521 --> 00:10:49,288
We slept together.

224
00:10:49,628 --> 00:10:51,656
Leslie? No.

225
00:10:55,645 --> 00:10:58,414
Oh, my God.
You know what?

226
00:10:58,487 --> 00:11:00,959
Yeah, we did, like,
five years ago. I sort of...

227
00:11:03,468 --> 00:11:06,842
No, but yeah.
Yeah, we did.

228
00:11:07,246 --> 00:11:09,217
Honey, can you straighten
up your area a little bit?

229
00:11:09,285 --> 00:11:11,690
'Cause they're
gonna be here soon.

230
00:11:11,759 --> 00:11:14,928
Can... Thanks.

231
00:11:15,002 --> 00:11:16,836
That parks lady is coming over,

232
00:11:16,907 --> 00:11:19,643
and we're gonna go
take a look at the pit.

233
00:11:19,716 --> 00:11:22,713
<i>I think it's probably</i>
<i>just a photo op for her.</i>

234
00:11:22,792 --> 00:11:25,128
<i>I mean, I don't think anybody in</i>
<i>government actually cares about...</i>

235
00:11:25,198 --> 00:11:27,363
Honey, do you mind doing
that somewhere else?

236
00:11:27,439 --> 00:11:29,103
I'm trying to watch TV.

237
00:11:29,177 --> 00:11:30,977
I'm talking about
the pit you fell in.

238
00:11:32,186 --> 00:11:34,181
It's Ms. Perkins<i>'</i> house.

239
00:11:37,234 --> 00:11:38,396
Doorbell!

240
00:11:38,471 --> 00:11:39,804
Yeah, I heard it.

241
00:11:42,716 --> 00:11:43,707
Doorbell!

242
00:11:43,786 --> 00:11:45,519
I heard it.
I'm getting it.

243
00:11:47,230 --> 00:11:49,361
- I'm here.
- Hey! Wow!

244
00:11:49,436 --> 00:11:51,271
- Look at that.
- Safety.

245
00:11:51,323 --> 00:11:52,845
You remember Tom
from last night.

246
00:11:52,860 --> 00:11:54,473
- Hey, Ann, how are you?
- Yeah, hey. Of course I do.

247
00:11:54,488 --> 00:11:56,186
And this is our college intern, April.

248
00:11:56,222 --> 00:11:58,559
She's going to be documenting
our fact-finding mission.

249
00:11:58,630 --> 00:11:59,894
- Hey, April.
- Hey.

250
00:11:59,966 --> 00:12:01,733
Is this fun for you?

251
00:12:01,806 --> 00:12:03,367
Yeah, it's so much fun.

252
00:12:03,444 --> 00:12:06,476
I'm just gonna grab my phone quickly.
Sorry.

253
00:12:10,297 --> 00:12:12,235
This must be our hero.

254
00:12:13,206 --> 00:12:15,041
The man heard 'round the world.

255
00:12:15,112 --> 00:12:16,445
Uh-huh.

256
00:12:16,515 --> 00:12:17,746
How you doing, son?

257
00:12:17,819 --> 00:12:18,947
Pretty good.

258
00:12:19,022 --> 00:12:20,686
I'm Leslie Knope.

259
00:12:21,630 --> 00:12:26,372
And the entire government of
Pawnee would like to let you know

260
00:12:26,445 --> 00:12:29,442
that we will do everything
we can to help you.

261
00:12:29,521 --> 00:12:30,990
Could you pass me
my itch stick?

262
00:12:31,058 --> 00:12:32,493
Of course.

263
00:12:47,105 --> 00:12:50,638
So, Andy jumped
over this fence.

264
00:12:50,716 --> 00:12:55,161
He was crossing through to get home,
and then he fell right there.

265
00:12:55,229 --> 00:12:58,432
Oh, my God.
How did we let this happen?

266
00:13:00,009 --> 00:13:01,674
Dream with me
for a second, Ann.

267
00:13:01,748 --> 00:13:05,213
Doesn't this neighborhood
deserve a first-class park?

268
00:13:05,325 --> 00:13:09,736
Imagine a shiny new playground
with a jungle gym and swings,

269
00:13:09,804 --> 00:13:14,182
pool, tennis courts,
volleyball courts, racquetball courts,

270
00:13:14,251 --> 00:13:16,450
basketball court,
regulation football field.

271
00:13:16,524 --> 00:13:19,694
We could put an amphitheatre over
there with Shakespeare in the Park.

272
00:13:19,768 --> 00:13:21,329
It's really not
that big of a pit.

273
00:13:21,406 --> 00:13:23,810
We could do
some of those things.

274
00:13:23,880 --> 00:13:28,154
It's gonna take a little extra work.
But why not try?

275
00:13:29,329 --> 00:13:30,924
I think that would be great.

276
00:13:31,000 --> 00:13:32,265
Me, too.

277
00:13:33,207 --> 00:13:34,506
I'm going in.

278
00:13:34,577 --> 00:13:36,515
- Why?
- Don't worry. I have a hard hat on.

279
00:13:36,583 --> 00:13:38,247
- I can see that, but...
- Yeah.

280
00:13:38,322 --> 00:13:39,814
- April, document this.
- Document what?

281
00:13:39,893 --> 00:13:41,157
The key to any
fact-finding mission

282
00:13:41,230 --> 00:13:44,399
is to get right into
the battle zone, you know?

283
00:13:44,473 --> 00:13:46,810
It's like George Bush
when he flew over New Orleans

284
00:13:46,880 --> 00:13:50,151
or Richard Nixon
when he went to China

285
00:13:50,224 --> 00:13:53,062
to see what the Chinese
were up to. No!

286
00:13:57,077 --> 00:13:58,980
Leslie, are you okay?

287
00:14:02,526 --> 00:14:05,969
Leslie! Hold on, April's gonna
get some photos for the website.

288
00:14:06,036 --> 00:14:07,164
Could you just be still?

289
00:14:07,239 --> 00:14:09,268
Don't move around as much.

290
00:14:09,345 --> 00:14:12,081
Okay, squeeze
my fingers. Good.

291
00:14:14,595 --> 00:14:16,190
- Does it hurt a lot?
- No.

292
00:14:16,266 --> 00:14:17,964
Mmm-mmm.
I'm fine.

293
00:14:18,473 --> 00:14:19,669
No, you're not.

294
00:14:19,744 --> 00:14:21,978
Good thing I was
wearing that hard hat.

295
00:14:22,050 --> 00:14:23,680
But it...
It fell off.

296
00:14:24,491 --> 00:14:26,359
After my head hit that rod.

297
00:14:27,733 --> 00:14:28,793
Wow.

298
00:14:28,870 --> 00:14:30,865
I can't believe you fell in, too.
That's awesome.

299
00:14:30,942 --> 00:14:32,572
It's not awesome, Andy.

300
00:14:32,648 --> 00:14:35,679
Well, at least my boss will listen
to me now that I broke my clavicle.

301
00:14:35,757 --> 00:14:37,751
- It's not broken.
- It is.

302
00:14:38,264 --> 00:14:42,038
Do you have one of those neck
foam collar brace things?

303
00:14:42,109 --> 00:14:43,773
Honestly, you're fine.

304
00:14:43,847 --> 00:14:46,320
Honestly, my clavicle's broken.

305
00:14:47,647 --> 00:14:49,189
Oh, hey, baby,
if you're going to the kitchen,

306
00:14:49,215 --> 00:14:51,181
could you make me
pancakes real quick?

307
00:14:51,235 --> 00:14:52,535
Sure.

308
00:14:52,606 --> 00:14:54,840
Ooh! Are pancakes
being made?

309
00:14:56,016 --> 00:14:57,646
- Thank you.
- Sure.

310
00:14:59,426 --> 00:15:02,891
I don't know, she's a little doofy,
but she's sweet.

311
00:15:06,413 --> 00:15:08,043
<i>Ron, please.</i>

312
00:15:08,118 --> 00:15:09,348
No. No way.

313
00:15:09,422 --> 00:15:12,055
Come on, Ron.
I've been a loyal foot soldier.

314
00:15:12,130 --> 00:15:15,572
Give me my shot.
Let me have Lot 48.

315
00:15:16,342 --> 00:15:18,678
Is that a travel pillow
around your neck?

316
00:15:18,750 --> 00:15:20,949
Ron, I don't know
how to explain this to you.

317
00:15:21,023 --> 00:15:24,123
When you've been down in the pit...
Have you been in the pit?

318
00:15:24,198 --> 00:15:26,136
No, I haven't gotten
down there yet.

319
00:15:26,204 --> 00:15:27,230
Well, I have.

320
00:15:27,308 --> 00:15:28,299
When you fell in.

321
00:15:28,377 --> 00:15:32,549
When I visited the bottom of the
pit on a fact-finding mission.

322
00:15:33,158 --> 00:15:38,972
And when you're down there, you get some
perspective about what it all means.

323
00:15:39,376 --> 00:15:41,645
And let me
tell you something, Ron.

324
00:15:41,717 --> 00:15:46,094
What it means is
I want this subcommittee.

325
00:15:48,503 --> 00:15:50,441
I've been quite open about
this around the office.

326
00:15:50,509 --> 00:15:53,484
I don't want this Parks
Department to build any parks

327
00:15:53,551 --> 00:15:55,181
because I don't
believe in government.

328
00:15:55,256 --> 00:15:59,599
I think that all government
is a waste of taxpayer money.

329
00:15:59,669 --> 00:16:04,411
My dream is to have the
park system privatized

330
00:16:04,617 --> 00:16:07,615
and run entirely for profit
by corporations.

331
00:16:08,462 --> 00:16:10,296
Like Chuck E. Cheese.

332
00:16:11,003 --> 00:16:12,837
They have an impeccable
business model.

333
00:16:12,908 --> 00:16:15,108
I would rather work
for Chuck E. Cheese.

334
00:16:15,182 --> 00:16:18,351
Well, I will definitely
think about it.

335
00:16:19,093 --> 00:16:20,529
I like the sound
of that definitely.

336
00:16:20,597 --> 00:16:23,538
I'm gonna leave before
you change your mind.

337
00:16:33,978 --> 00:16:38,617
Mr. Swanson, I presume.
Lot 48. Thoughts? Yes? No? Me?

338
00:16:38,692 --> 00:16:39,992
Park? Giving
the park to me?

339
00:16:40,063 --> 00:16:41,123
Still mulling.

340
00:16:41,199 --> 00:16:42,567
<i>She's insatiable.</i>

341
00:16:42,637 --> 00:16:47,048
She's like a little dog
with a chew toy.

342
00:16:47,116 --> 00:16:49,111
All right, you guys. All right,
all right. I'll ask him.

343
00:16:49,189 --> 00:16:50,820
Everybody wants to know
what your decision is.

344
00:16:50,895 --> 00:16:52,763
Someone on the phone
about the park.

345
00:16:52,833 --> 00:16:55,603
You know,
I don't think I even want it.

346
00:16:56,878 --> 00:17:00,081
Oh. Man, my clavicle's
still really hurting me.

347
00:17:04,668 --> 00:17:06,138
Oh! This is
a great shot.

348
00:17:06,206 --> 00:17:07,871
I know.
That's awesome.

349
00:17:07,945 --> 00:17:10,475
I think she's crying in that one.
Look at this one.

350
00:17:10,552 --> 00:17:12,022
- Hey, kids.
- That's another good one.

351
00:17:12,090 --> 00:17:13,993
- Hey.
- Hey, Brendanawicz.

352
00:17:14,062 --> 00:17:15,589
You gotta come check this out.

353
00:17:15,667 --> 00:17:17,604
Leslie took us out
to that pit in Lot 48,

354
00:17:17,672 --> 00:17:19,108
and she fell inside.

355
00:17:19,177 --> 00:17:20,579
- And we have some awesome photos.
- One.

356
00:17:20,648 --> 00:17:22,449
- Is she okay?
- Yeah, she's fine.

357
00:17:22,521 --> 00:17:23,512
Oh! This is a classic.

358
00:17:24,192 --> 00:17:25,252
That's the best one.

359
00:17:25,328 --> 00:17:27,961
- The up-skirt photo.
- Awesome.

360
00:17:30,711 --> 00:17:34,849
Hey, man. Give me the photo back.
What's going on?

361
00:17:36,260 --> 00:17:37,730
Don't worry.
I can print more.

362
00:17:39,604 --> 00:17:41,940
Leslie is unique.

363
00:17:42,847 --> 00:17:45,845
<i>Government work</i>
<i>can beat you down.</i>

364
00:17:45,922 --> 00:17:50,595
<i>I would say that I lost my optimism</i>
<i>about government in about two months.</i>

365
00:17:51,339 --> 00:17:53,777
Leslie's kept hers
for six years.

366
00:17:54,581 --> 00:17:58,183
I've got a few rules about how to
survive and prosper in a government job,

367
00:17:58,259 --> 00:18:01,496
and I think I'm about to
break one of those rules.

368
00:18:02,872 --> 00:18:04,604
I want you to give
Lot 48 to Leslie

369
00:18:04,677 --> 00:18:06,672
so she can try to build a park.

370
00:18:08,522 --> 00:18:09,650
Why should I?

371
00:18:09,725 --> 00:18:11,720
You owe me one, remember?

372
00:18:11,798 --> 00:18:14,101
Do you want to
cash in for this?

373
00:18:14,807 --> 00:18:16,437
Yeah, I do.

374
00:18:17,047 --> 00:18:20,751
<i>So this was built in 1935.</i>
<i>It's called Pioneer Hall.</i>

375
00:18:21,292 --> 00:18:22,694
<i>And a little trivia,</i>

376
00:18:22,763 --> 00:18:26,001
<i>it is one of the first structures</i>
<i>in America to ever have locks.</i>

377
00:18:26,642 --> 00:18:27,633
Wow!

378
00:18:27,712 --> 00:18:29,843
Oh, yeah.
This is our crown jewel.

379
00:18:29,918 --> 00:18:31,320
It's one of our best murals.

380
00:18:31,389 --> 00:18:34,558
It depicts the very famous
battle at Conega Creek.

381
00:18:34,631 --> 00:18:36,067
We have a lot of
children visit,

382
00:18:36,136 --> 00:18:40,639
so often we have to cover up the
more gruesome parts with a poster.

383
00:18:41,018 --> 00:18:43,422
That is horrifying.

384
00:18:43,491 --> 00:18:45,052
Yes, it is.

385
00:18:46,433 --> 00:18:48,770
So, what do you want to see?
The DMV? Animal control?

386
00:18:48,840 --> 00:18:50,640
- Or we could...
- Leslie!

387
00:18:54,992 --> 00:18:58,126
We did it.
Ron's approved our committee.

388
00:18:58,535 --> 00:19:00,564
Oh, my God.
That's great!

389
00:19:00,641 --> 00:19:02,545
- That's so great.
- That's so exciting!

390
00:19:02,613 --> 00:19:04,140
This is huge.

391
00:19:04,218 --> 00:19:05,654
I'm barely 34,

392
00:19:05,722 --> 00:19:09,130
and I've already landed a Parks
Department exploratory subcommittee.

393
00:19:09,200 --> 00:19:11,696
I'm a rocket ship.

394
00:19:11,774 --> 00:19:14,213
- Leslie. I just heard the news.
- Hey.

395
00:19:14,281 --> 00:19:15,774
- Isn't it great?
- Congratulations.

396
00:19:15,853 --> 00:19:18,554
Thank you. I'm so excited.
It's exciting, isn't it?

397
00:19:18,588 --> 00:19:19,579
- It is.
- Yeah.

398
00:19:19,597 --> 00:19:20,931
Should I call a press
conference now, or...

399
00:19:20,957 --> 00:19:22,221
- Yeah, call one. Definitely.
- I would.

400
00:19:22,238 --> 00:19:24,437
Every now and then we have
these little gatherings,

401
00:19:24,512 --> 00:19:27,578
and Leslie gets plastered.

402
00:19:27,654 --> 00:19:31,256
<i>One time I convinced her to try</i>
<i>to fax someone a Fruit Roll-Up.</i>

403
00:19:31,331 --> 00:19:35,902
<i>She one time made out with the</i>
<i>water-delivery guy in her office.</i>

404
00:19:35,978 --> 00:19:38,884
<i>On Halloween,</i>
<i>she was dressed up as Batman.</i>

405
00:19:38,987 --> 00:19:40,958
<i>Not Batgirl. Batman.</i>

406
00:19:41,027 --> 00:19:44,834
<i>And I convinced her to go stop a</i>
<i>crime that was going on outside.</i>

407
00:19:44,904 --> 00:19:47,434
And it is my favorite thing
in the world.

408
00:19:47,512 --> 00:19:49,484
You know what?
America is awesome.

409
00:19:49,551 --> 00:19:52,583
<i>It's so full of hope</i>
<i>and small towns</i>

410
00:19:52,660 --> 00:19:54,997
<i>and big cities and real people</i>

411
00:19:55,068 --> 00:19:57,701
and delicious beverages
and hot guys.

412
00:19:58,277 --> 00:20:00,374
You just never know when
opportunity is gonna strike.

413
00:20:00,450 --> 00:20:01,680
- Yeah.
- You gotta be ready for it.

414
00:20:01,754 --> 00:20:02,882
- Yeah.
- Are you excited?

415
00:20:02,957 --> 00:20:04,153
Definitely am.
Fired up.

416
00:20:04,227 --> 00:20:05,697
- Yeah!
- I'm really fired up.

417
00:20:05,765 --> 00:20:08,364
I... You know they say
that democracy or whatever

418
00:20:08,439 --> 00:20:12,007
only works when people get involved?
Well, I'm getting involved.

419
00:20:12,084 --> 00:20:15,287
So I am gonna make a vow now

420
00:20:15,360 --> 00:20:19,201
that I will do whatever it
takes to get this pit filled in.

421
00:20:19,271 --> 00:20:21,106
Even if it takes two months.

422
00:20:22,280 --> 00:20:26,520
<i>Soul sista, soul sista
Gonna get your phone, sista</i>

423
00:20:26,593 --> 00:20:32,372
Yeah.
<i>Sweet Lady Marmalard</i>

424
00:20:35,344 --> 00:20:37,909
I've created this office
as a symbol

425
00:20:37,986 --> 00:20:39,752
of how I feel about government.

426
00:20:39,824 --> 00:20:43,494
This sawed-off shotgun
belonged to a local bootlegger.

427
00:20:43,568 --> 00:20:46,133
People who come in here
to ask me for things

428
00:20:46,209 --> 00:20:48,341
have to stare
right down the barrel.

429
00:20:48,416 --> 00:20:50,912
Did you guys get
a grant to do this?

430
00:20:50,991 --> 00:20:52,655
This is my basketball court.

431
00:20:52,729 --> 00:20:54,427
I don't want to see
any double dribble.

432
00:20:54,501 --> 00:20:57,030
I don't want to see
any three-second violations.

433
00:20:57,109 --> 00:20:58,305
Bobby Knight!

