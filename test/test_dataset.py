from parameterized import parameterized
import pytest
import os

from autosync.dataset import *

@parameterized.expand([
    ('(noise)', True),
    ('(noise) Hey Pal.', False),
    ('{\\pos(12, 10)} Hey Pal.', False),
    ('{\\an1} Hey Pal.', False),
    ('♪', True),
    ('♪♪', True),
    ('♪ Hey, Pal ♪', True),
    ('A', True),
    ('AA', True),
    ('AAA', False),
    ('', True),
    ('Synced & corrected by <foo>\nwww.addic7ed.com', True),
])
def test_ignore_sequence(sentence, expected):
    assert ignore_sequence(sentence) == expected

@pytest.fixture
def sub_filename():
    return os.path.join('test', 'resource', 'Parks.and.Recreation.S01E01.srt')

def test_sub_to_sequence(sub_filename):
    seq = sub_to_sequence(sub_filename, total_length=1000000)
    assert np.isclose(np.amin(seq), 0.0)
    assert np.isclose(np.amax(seq), 1.0)

@parameterized.expand([
    ('test/resource/filelist/foo.mkv', True, 1),
    ('test/resource/filelist/bar[qux].mkv', True, 0),
    ('test/resource/filelist/bar[qux].mkv', False, 1),
    ('test/resource/filelist/*.mkv', True, 2),
])
def test_detect_files(dirlist, globbing, expected_count):
    result = list_files(dirlist, globbing=globbing)
    assert len(result) == expected_count
